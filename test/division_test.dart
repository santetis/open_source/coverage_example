@TestOn('vm')
import 'package:coverage_playground/coverage_playground.dart';
import 'package:test/test.dart';

void main() {
  test('division', () {
    expect(division(12, 2), 6);
  });

  test('division by 0', () {
    expect(
      division(12, 0),
      double.infinity,
    );
  });

  test('0 divided by 12', () {
    expect(division(0, 12), 0);
  });
}
