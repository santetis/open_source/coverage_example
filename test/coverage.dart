import 'addition_test.dart' as addition;
import 'division_test.dart' as division;
import 'multiplication_test.dart' as multiplication;
import 'substraction_test.dart' as substraction;

void main() {
  addition.main();
  division.main();
  multiplication.main();
  substraction.main();
}
