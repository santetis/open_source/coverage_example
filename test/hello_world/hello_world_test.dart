@TestOn('browser')
import 'package:coverage_playground/coverage_playground_html.dart';
import 'package:test/test.dart';
import 'package:angular_test/angular_test.dart';
import 'package:pageloader/html.dart';
import 'hello_world_page_object.dart';
// ignore: uri_has_not_been_generated
import 'hello_world_test.template.dart' as ng;

void main() {
  ng.initReflector();
  final testBed = new NgTestBed<HelloWorldComponent>();
  NgTestFixture<HelloWorldComponent> fixture;
  HelloWorldPageObject helloWorldPageObject;

  tearDown(disposeAnyRunningTest);

  setUp(() async {
    fixture = await testBed.create();
    final context =
        HtmlPageLoaderElement.createFromElement(fixture.rootElement);
    helloWorldPageObject = HelloWorldPageObject.create(context);
  });

  test('should render "Hello World"', () async {
    expect(helloWorldPageObject.name, 'World');
  });

  test('should render "Hello Kleak"', () async {
    await fixture.update((c) => c.name = 'Kleak');
    expect(helloWorldPageObject.name, 'Kleak');
  });
}
