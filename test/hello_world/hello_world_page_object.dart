import 'package:pageloader/html.dart';

part 'hello_world_page_object.g.dart';

@PageObject()
abstract class HelloWorldPageObject {
  HelloWorldPageObject();

  factory HelloWorldPageObject.create(PageLoaderElement context) =
      $HelloWorldPageObject.create;

  @ById('name')
  PageLoaderElement get _name;

  String get name => _name.visibleText;
}
