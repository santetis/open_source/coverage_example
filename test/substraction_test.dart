@TestOn('vm')
import 'package:coverage_playground/coverage_playground.dart';
import 'package:test/test.dart';

void main() {
  test('substraction', () {
    expect(substraction(2, 1), 1);
  });
}
