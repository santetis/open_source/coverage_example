@TestOn('vm')
import 'package:coverage_playground/coverage_playground.dart';
import 'package:test/test.dart';

void main() {
  test('addition', () {
    expect(addition(1, 1), 2);
  });
}
