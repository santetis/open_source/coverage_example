import 'package:angular/angular.dart';

@Component(
  selector: 'test',
  templateUrl: 'hello_world.html',
)
class HelloWorldComponent {
  String name = 'World';
}
