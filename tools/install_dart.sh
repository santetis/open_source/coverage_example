#!/bin/bash

apt-get install -qq apt-transport-https curl gnupg
sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'
sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_unstable.list > /etc/apt/sources.list.d/dart_unstable.list'
apt-get update -qq
apt-get install -qq dart
