# Check file are well formatted
unformatted=$(dartfmt -n --set-exit-if-changed lib/ test/)
if [ -n "$unformatted" ]; then
  echo >&2 "dart files must be formatted with dartfmt. Please run:"
  for fn in $unformatted; do
    echo >&2 "  dartfmt -w $PWD/$fn"
  done
  exit 1
fi

pub run build_runner build

analyzed=$(dartanalyzer --options=analysis_options.yaml --fatal-hints --fatal-warnings .)
retValue=$?
if [ $retValue -ne 0 ]; then
  echo "${analyzed}"
  exit 1;
fi

pub run build_runner test -- -p chrome