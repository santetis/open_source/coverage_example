OBS_PORT=9292
echo "Collecting coverage on port $OBS_PORT..."
#   Start test in one VM
dart \
    --enable-vm-service=$OBS_PORT \
    --pause-isolates-on-exit \
    test/coverage.dart &

# Run the coverage collector to generate the JSON coverage report.
collect_coverage \
    --port=$OBS_PORT \
    --out=var/coverage.json \
    --wait-paused \
    --resume-isolates

echo "Generating LCOV report..."
format_coverage \
    --lcov \
    --in=var/coverage.json \
    --out=var/lcov.info \
    --packages=.packages \
    --report-on=lib

genhtml -o coverage var/lcov.info